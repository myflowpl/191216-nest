import { Controller, Get } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { PhotosService, CreateThumbsPattern } from '@app/photos-core';

@Controller()
export class AppController {

  constructor(private readonly photosService: PhotosService) {}

  @MessagePattern(CreateThumbsPattern.pattern)

  accumulate(data: CreateThumbsPattern) {
    return this.photosService.createThumbs(data.filename);
  }
}
