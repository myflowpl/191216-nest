import { config as dotenv } from 'dotenv';
dotenv();
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.TCP,
    options: {
      port: parseInt(process.env.PORT, 10) || 3001,
    },
  });
  app.listen(() => console.log('Microservice is listening'));
}
bootstrap();
