import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { PhotosCoreModule } from '@app/photos-core';

@Module({
  imports: [PhotosCoreModule],
  controllers: [AppController],
})
export class AppModule {}
