import { ApiProperty } from '@nestjs/swagger';

export class CommentModel {
  @ApiProperty()
  id: number;
  @ApiProperty({
    example: 'Piotr',
    description: 'Name dla usera',
  })
  name: string;
}
