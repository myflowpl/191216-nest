import { CommentModel } from '../model';
import { GetCommentsRequestDto } from './get-comments-request.dto';

export class GetCommentsResponseDto {
  pageIndex: number;
  pageSize: number;
  total: number;
  data: CommentModel[];
  query: GetCommentsRequestDto;
}
