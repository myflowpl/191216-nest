import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class GetCommentsRequestDto {
  @ApiPropertyOptional()
  search: string;
  @ApiPropertyOptional()
  pageIndex: number;
  @ApiPropertyOptional()
  pageSize: number;
}
