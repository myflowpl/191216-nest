import { CommentModel } from '../model';

export interface GetCommentResponseDto {
  total: number;
  data: CommentModel;
}
