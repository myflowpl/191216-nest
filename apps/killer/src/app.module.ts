import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CommentsModule } from './comments/comments.module';
import { UserModule } from './user/user.module';
import { ConfigModule } from '@app/config';
import { APP_FILTER } from '@nestjs/core';
import { HttpExceptionFilter } from './shared/http-exception.filter';
import { PhotosModule } from './photos/photos.module';
import { DbModule } from '@app/db';
import { ChatGateway } from './gateways/chat.gateway';

@Module({
  imports: [CommentsModule, UserModule, ConfigModule, PhotosModule, DbModule],
  controllers: [AppController],
  providers: [AppService, ChatGateway,

  ],
})
export class AppModule { }
