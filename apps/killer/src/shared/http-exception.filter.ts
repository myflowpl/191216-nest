import { ExceptionFilter, Catch, ArgumentsHost, HttpException, ForbiddenException } from '@nestjs/common';
import { UserService } from '../user/services';

@Catch(ForbiddenException)
export class HttpExceptionFilter implements ExceptionFilter {

  // constructor(private userService: UserService) {
    // console.log('user service in filter', userService);
  //  }

  catch(exception, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const status = exception.status || 500;

    console.log('ERROR', exception);
    console.log('message', exception.message);

    response
      .status(status)
      .json({
        statusCode: status,
        timestamp: new Date().toISOString(),
        path: request.url,
        data: process.env.DEBUG ? exception.message : null,
      });
  }
}
