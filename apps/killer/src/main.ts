import 'source-map-support/register';
import { config as dotenv } from 'dotenv';
dotenv();
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ExpressAdapter } from '@nestjs/platform-express';
// import { express } from '../express/server.js';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { UserService } from './user/services/user.service';
import { Injector } from '@nestjs/core/injector/injector';
import { ConfigService } from '@app/config';

async function bootstrap() {
  // const app = await NestFactory.create(AppModule, new ExpressAdapter(express));
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('api');
  // app.enableCors();
  app.enableShutdownHooks();

  const config = app.get(ConfigService);
  const options = new DocumentBuilder()
    .setTitle('Nest API Example')
    .setDescription('Przykładowy projekt w Node.js i TypeScript')
    .setVersion('1.0')
    .addTag('user')
    .addBearerAuth({
      type: 'apiKey',
      in: 'header',
      name: config.TOKEN_HEADER_NAME,
    })
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/docs', app, document);

  await app.listen(3000);
}
bootstrap();

// function Log(label) {
//   return (target, key, descriptor) => {

//     console.log('INIT Log decorator', label)
//     const tmp = target[key];
//     target[key] = () => {
//       console.log(`${label}: ${key} was called!`);
//       tmp();
//     }
//   };
// }

// class P {
//   @Log('debug')
//   foo() {
//     console.log('FOO')
//   }

//   @Log('wargning')
//   bar() {

//   }
// }
// setTimeout(() => {
//   const p = new P();
//   setTimeout(() => {
//     p.foo();
//   }, 2000);
// }, 2000);
