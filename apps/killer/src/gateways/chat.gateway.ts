import { SubscribeMessage, WebSocketGateway, OnGatewayInit, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { Logger } from '@nestjs/common';

@WebSocketGateway({ namespace: '/chat' })
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

  handleConnection(client: any, ...args: any[]) {
    this.users.push(client);
  }
  
  handleDisconnect(client: any) {
    this.users = this.users.filter(c =>  c !== client);
  }

  @WebSocketServer() wss: Server;

  private users = [];
  private logger: Logger = new Logger('ChatGateway');

  afterInit(server: any) {
    this.logger.log('Initialized!');

    setInterval(() => {
      this.wss.emit('chatToClient', {
        sender: 'SERVER',
        message: `Users count: ${this.users.length}`,
      });
    }, 5000);
  }

  @SubscribeMessage('chatToServer')
  handleMessage(client: Socket, message: { sender: string, message: string }) {
    this.wss.emit('chatToClient', message);
  }

}
