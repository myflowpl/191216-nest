import { UserMiddleware } from './user.middleware';
import { AuthService } from '../services';
import { ConfigService } from '@app/config';
import * as express from 'express';
import { TokenPayloadModel } from '../models';

describe('UserMiddleware', () => {

  const payloadMock = {
    user: {},
  }
  const authServiceMock: Partial<AuthService> = {
    async tokenVerify(token: string): Promise<TokenPayloadModel> {
      console.log('VERIFY', token)
      return payloadMock as any;
    },
  };
  const configServiceMock: Partial<ConfigService> = {
    TOKEN_HEADER_NAME: 'auth',
  };
  let middleware: UserMiddleware;

  beforeEach(() => {
    middleware = new UserMiddleware(
      configServiceMock as ConfigService,
      authServiceMock as AuthService,
    );
  });

  it('should be defined', () => {
    expect(middleware).toBeDefined();
  });

  it('should extend request with the payload decoded from token', async () => {
    const req: Partial<express.Request> = {
      headers: {
        [configServiceMock.TOKEN_HEADER_NAME]: 'sldjfjsdfksdfsdf',
      },
    };
    await middleware.use(req as express.Request, {}, () => { });
    expect(req.tokenPayload).toMatchObject(payloadMock);
  });

});
