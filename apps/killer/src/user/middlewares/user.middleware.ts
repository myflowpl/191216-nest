import { Injectable, NestMiddleware } from '@nestjs/common';
import { ConfigService } from '@app/config';
import { AuthService } from '../services';
import * as express from 'express';

@Injectable()
export class UserMiddleware implements NestMiddleware {

  constructor(
    private config: ConfigService,
    private authService: AuthService,
  ) {}

  async use(req: express.Request, res, next) {
    if (req.headers[this.config.TOKEN_HEADER_NAME]) {
      const payload = await this.authService.tokenVerify(req.headers[this.config.TOKEN_HEADER_NAME] as string);
      if (payload) {
        req.tokenPayload = payload;
      }
    }
    next();
  }
}
