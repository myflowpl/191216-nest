import { Controller, Post, Body, Request, Get, UseGuards, HttpException, HttpStatus, UsePipes, ValidationPipe, Query, Param, ParseIntPipe, NotFoundException, UseInterceptors } from '@nestjs/common';
import { UserService } from '../services/user.service';
import { ApiCreatedResponse, ApiBearerAuth, ApiParam } from '@nestjs/swagger';
import { UserRegisterResponseDto, UserRegisterRequestDto, UserLoginRequestDto, UserLoginResponseDto } from '../dto';
import { UserModel, UserRole } from '../models';
import { Roles, User } from '../decorators';
import { AuthGuard } from '../guards';
import { AuthService } from '../services';
import { UserByIdPipe } from '../pipes/user-by-id.pipe';
import { UserInterceptor } from '../interceptors/user.interceptor';
import { ClientProxy, Transport, Client } from '@nestjs/microservices';
import { Observable } from 'rxjs';

@Controller('user')
export class UserController {

  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) { }

  @Client({ transport: Transport.TCP, options: {port: 3001} })
  client: ClientProxy;

  @Get('sum')
  sum(): Observable<number> {
    const pattern = { cmd: 'sum' };
    const payload = [1, 2, 3];
    return this.client.send<number>(pattern, payload);
  }

  @Post('login')
  @UsePipes(new ValidationPipe({ transform: true }))
  @ApiCreatedResponse({ type: UserLoginResponseDto })
  async login(@Body() credentials: UserLoginRequestDto): Promise<UserLoginResponseDto> {

    const user = await this.userService.findByCredentials(credentials.email, credentials.password);

    if (!user) {
      throw new HttpException('ValidationError', HttpStatus.UNPROCESSABLE_ENTITY);
    }
    return {
      token: await this.authService.tokenSign({ user }),
      user,
    };
  }

  @Post('register')
  @ApiCreatedResponse({ type: UserRegisterResponseDto })
  async register(
    @Body() data: UserRegisterRequestDto,
  ): Promise<UserRegisterResponseDto> {

    const user = await this.userService.create(data);

    // TODO handle errors
    return {
      user,
    };
  }

  @Get()
  @ApiBearerAuth()
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard)
  getUser(@User() user: UserModel) {
    return {
      user,
    };
  }

  @ApiParam({ name: 'id' })
  @Get('/pipe/:id')
  async getUserByIdWithPipe(@Param('id', new ParseIntPipe()) id: number) {
    const user = await this.userService.getById(id);
    if (!user) {
      throw new NotFoundException('user not found');
    }
    return user;
  }

  @ApiParam({ name: 'id', type: Number })
  @Get(':id')
  @UseInterceptors(UserInterceptor)
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  async getUserByid(@Param('id', UserByIdPipe) user: UserModel) {
    console.log('handler');
    // buisnes logic
    return {user};
  }

}
