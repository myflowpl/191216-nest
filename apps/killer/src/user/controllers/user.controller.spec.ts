import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { AuthService, UserService } from '../services';

describe('User Controller', () => {
  let controller: UserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [{
        provide: AuthService,
        useValue: {},
      }, {
        provide: UserService,
        useValue: {},
      }],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
