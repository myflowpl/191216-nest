import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class UserInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {

    const req = context.switchToHttp().getRequest();
    console.log('intercept before', req.url);
    
    // modify request before handler
    return next.handle().pipe(
      map(data => {
        // modify it after
        console.log('intercept after', data);
        return data;
      }),
    );

  }
}
