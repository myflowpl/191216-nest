import { Module, Global, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { logger } from './middlewares/logger.middleware';
import { UserMiddleware } from './middlewares/user.middleware';
import { APP_GUARD, APP_FILTER } from '@nestjs/core';
import { AuthGuard } from './guards';
import { AuthService } from './services/auth.service';
import { HttpExceptionFilter } from '../shared/http-exception.filter';

// @Global()
@Module({
  controllers: [UserController],
  providers: [
    UserService,
    AuthService,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    //   {
    //   provide: APP_GUARD,
    //   useClass: AuthGuard,
    // }
  ],
  exports: [UserService],
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(logger, UserMiddleware).forRoutes(UserController);
    // consumer.apply(authMiddlewa).forRoutes({
    //   path: 'user',
    //   method: RequestMethod.ALL,
    // });
  }
}
