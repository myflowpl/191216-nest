import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export enum UserRole {
  ADMIN = 'admin',
  ROOT = 'root',
}
export class UserModel {
  @ApiPropertyOptional()
  id?: number;
  @ApiProperty()
  name: string;
  @ApiPropertyOptional()
  email?: string;
  @ApiPropertyOptional()
  password?: string;
  @ApiPropertyOptional({
    enum: UserRole,
    isArray: true,
  })
  roles?: UserRole[];
}

export class TokenPayloadModel {
  user: UserModel;
}
