import { createParamDecorator } from '@nestjs/common';
import * as express from 'express';

export function userDecoratorFactory(data: any, req: express.Request) {
  return (req.tokenPayload && req.tokenPayload.user) ? req.tokenPayload.user : undefined;
}

export const User = createParamDecorator(userDecoratorFactory);
