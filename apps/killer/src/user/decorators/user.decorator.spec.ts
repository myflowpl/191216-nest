import { userDecoratorFactory } from './user.decorator';
import * as express from 'express';

describe('User Decorator', () => {
  const req: Partial<express.Request> = {
    tokenPayload: {
      user: {
        id: 1,
        name: 'Piotr',
      },
    },
  };
  it('should return user from request', () => {
    expect(userDecoratorFactory({}, req as express.Request)).toMatchObject(req.tokenPayload.user);
  });
  it('should return undefined request', () => {
    expect(userDecoratorFactory({}, {} as express.Request)).toBeUndefined();
  });
});
