import { UserModel } from '../models';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MinLength } from "class-validator";
export class UserRegisterRequestDto {
  @ApiProperty({
    example: 'Piotr',
  })
  name: string;
  @ApiProperty({
    example: 'piotr@myflow.pl',
  })
  email: string;
  @ApiProperty({
    example: '123',
  })
  password: string;
}

export class UserRegisterResponseDto {
  @ApiProperty()
  user: UserModel;
}

export class UserLoginRequestDto {
  @IsEmail()
  @ApiProperty({ example: 'piotr@myflow.pl' })
  email: string;

  @MinLength(3, {
    message: "Password is to short. Minimal length is $constraint1 characters, but actual is $value"
  })
  @IsString()
  @ApiProperty({ example: '123' })
  password: string;
}

export class UserLoginResponseDto {
  @ApiProperty()
  token: string;
  @ApiProperty({ type: UserModel })
  user: UserModel;
}
