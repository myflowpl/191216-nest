import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { ConfigModule, ConfigService } from '@app/config';
import { TokenPayloadModel } from '../models';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {

    const configMock: Partial<ConfigService> = {
      JWT_SECRET: 'jwt-secret-here',
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthService,
        {
          provide: ConfigService,
          useValue: configMock,
        },
      ],
    })
      .compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('token generation', async () => {
    const payload: TokenPayloadModel = {
        user: {
          id: 1,
          name: 'piotr',
          email: 'piotr@myflow.pl',
        },
      };
    const token = await service.tokenSign(payload);
    expect(typeof token).toBe('string');
    await expect(service.tokenDecode(token)).resolves.toMatchObject(payload);
  });

  it('token verify error', async () => {
    await expect(service.tokenVerify('sdlkjdslfk sid8iskdf')).resolves.toBeNull();
  });
});
