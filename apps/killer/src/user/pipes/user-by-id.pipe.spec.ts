import { UserByIdPipe } from './user-by-id.pipe';
import { NotFoundException, BadRequestException } from '@nestjs/common';
import { UserService } from '../services';

describe('UserByIdPipe', () => {

  const user = {
    id: 1,
    name: 'Piotr',
  };

  const userServiceMock: Partial<UserService> = {
    async getById(id: number) {
      if (id !== 1) {
        console.log('id', id);
        return null;
      }
      return user;
    },
  };
  let pipe: UserByIdPipe;

  beforeAll(() => {
    pipe = new UserByIdPipe(userServiceMock as UserService);
  });

  it('should return user', async () => {
    expect(await pipe.transform('1', {} as any)).toMatchObject(user);
  });

  it('should throw NotFoundException', () => {
    expect(pipe.transform('2', {} as any)).rejects.toThrow(NotFoundException);
  });

  it('should throw BadRequestException', () => {
    expect(pipe.transform('wefdfgf', {} as any)).rejects.toThrow(BadRequestException);
  });
});
