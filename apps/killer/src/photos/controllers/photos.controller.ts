import { Controller, Post, UseInterceptors, Body, UploadedFile, UploadedFiles, OnModuleInit, Get } from '@nestjs/common';
import { ApiConsumes, ApiBody, ApiProperty } from '@nestjs/swagger';
import { PhotosService, CreateThumbsPattern } from '@app/photos-core';
import { FileInterceptor } from '@nestjs/platform-express';
import { Transport, Client, ClientProxy } from '@nestjs/microservices';
import { Observable } from 'rxjs';

class FileUploadDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: any;
  @ApiProperty()
  description: string;
}

@Controller('photos')
export class PhotosController {

  @Client({ transport: Transport.TCP, options: { port: 3001 } })
  client: ClientProxy;

  constructor(private photosService: PhotosService) { }

  @Post('upload-user-avatar')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'List of cats',
    type: FileUploadDto,
  })
  async uploadFile(@UploadedFile() file: Express.Multer.File, @Body() body) {
    const avatar = await this.photosService.create(file, body.description);
    const thumb = await this.createThumbs(avatar.photo.filename).toPromise();
    return { avatar, thumb, file, body };
  }

  @Get()
  getPhotos() {
    return this.photosService.findAll();
  }

  createThumbs(filename: string): Observable<number> {

    const payload: CreateThumbsPattern = {
      filename,
    };
    return this.client.send<number>(CreateThumbsPattern.pattern, payload);
  }
}
