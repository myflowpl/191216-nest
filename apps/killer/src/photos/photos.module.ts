import { Module } from '@nestjs/common';
import { PhotosController } from './controllers/photos.controller';
import { PhotosService } from '@app/photos-core';
import { MulterModule } from '@nestjs/platform-express';
import { ConfigService } from '@app/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PhotoEntity } from '@app/db/entities';

@Module({
  imports: [
    TypeOrmModule.forFeature([PhotoEntity]),
    ServeStaticModule.forRoot({
      rootPath: join(process.env.STORAGE_DIR, 'assets'),
    }),
    MulterModule.registerAsync({
      useFactory: async (config: ConfigService) => ({
        dest: config.STORAGE_TMP,
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [PhotosController],
  providers: [PhotosService],
})
export class PhotosModule {}
