import { config } from 'dotenv';
config();
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { UserRegisterRequestDto, UserRegisterResponseDto, UserLoginRequestDto, UserLoginResponseDto } from '../src/user/dto';
import { TokenPayloadModel, UserRole } from '../src/user/models';
import { ConfigService } from '@app/config';
import { INestApplication } from '@nestjs/common';
import { AuthService } from '../src/user/services';

describe('UserController (e2e)', () => {
  let app: INestApplication;
  let conf: ConfigService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      // .overrideProvider(AuthService).useClass(AuthMockService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    conf = app.get(ConfigService);
  });

  it('/user/register (POST)', () => {
    const req: UserRegisterRequestDto = {
      name: 'piotr',
      email: 'piotr@myflow.pl',
      password: '123',
    };
    const res: UserRegisterResponseDto = {
      user: {
        id: expect.any(Number),
        name: 'piotr',
        email: 'piotr@myflow.pl',
      },
    };
    return request(app.getHttpServer())
      .post('/user/register')
      .send(req)
      .expect(201)
      .then(r => {
        expect(r.body).toMatchObject(res);
      });
  });

  let token;
  it('/user/login SUCCESS', () => {
    const req: UserLoginRequestDto = {
      email: 'piotr@myflow.pl',
      password: '123',
    };
    const resBody: UserLoginResponseDto = {
      token: expect.any(String),
      user: {
        id: expect.any(Number),
        name: 'Piotr',
        email: 'piotr@myflow.pl',
      },
    };
    return request(app.getHttpServer())
      .post('/user/login')
      .send(req)
      .expect(201)
      .then(res => {
        token = res.body.token;
        console.log(token)
        expect(res.body).toMatchObject(resBody);
      });
  });

  it('/user/login ERROR', () => {
    const req: UserLoginRequestDto = {
      email: 'piotr@myflow.pl',
      password: '0004',
    };

    return request(app.getHttpServer())
      .post('/user/login')
      .send(req)
      .expect(422);
  });

  it('/user (GET)', () => {
    const resBody: TokenPayloadModel = {
      user: {
        id: expect.any(Number),
        name: 'Piotr',
        email: 'piotr@myflow.pl',
        roles: [UserRole.ADMIN],
      },
    };
    const authService = app.get(AuthService);
    authService.tokenVerify = async function (token: string): Promise<TokenPayloadModel> {
      return {
        user: {
          ...resBody.user,
          id: 1,
        },
      };
    };
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJuYW1lIjoiUGlvdHIiLCJlbWFpbCI6InBpb3RyQG15Zmxvdy5wbCIsInBhc3N3b3JkIjoiMTIzIiwicm9sZXMiOlsiYWRtaW4iXX0sImlhdCI6MTU3NjU5NjQwM30.91eb2JCwAwwSbWWCmL0xU8lmQwgzTVbHzCI5n2pNOqk';
    return request(app.getHttpServer())
      .get('/user')
      .set(conf.TOKEN_HEADER_NAME, token)
      .expect(200)
      .then(res => {
        console.log('body', res.body)
        expect(res.body).toMatchObject(resBody);
      });
  });

  it('/user (GET) AuthGuard Forbidden', () => {
    return request(app.getHttpServer())
      .get('/user')
      .expect(403);
  });
});
