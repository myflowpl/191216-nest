import { Injectable, OnModuleInit, OnModuleDestroy, OnApplicationShutdown } from '@nestjs/common';
import { resolve } from 'path';

@Injectable()
export class ConfigService implements OnModuleInit, OnModuleDestroy, OnApplicationShutdown {

  onApplicationShutdown(signal?: string) {
    console.log('HOOK onApplicationShutdown');
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log('HOOK onApplicationShutdown DONE');
        resolve();
      }, 4000);
    });
  }

  async onModuleInit() {
    console.log('HOOK onModuleInit');
    if (!this.STORAGE_DIR) {
      throw new Error('STORAGE_DIR evn config is required! REMEMBER TO RENAME .env-tpl to .env !!!');
    }
  }
  onModuleDestroy() {
    console.log('HOOK onModuleDestroy');
    // throw new Error("Method not implemented.");
  }

  readonly STORAGE_DIR = process.env.STORAGE_DIR;

  readonly JWT_SECRET = process.env.JWT_SECRET;
  readonly TOKEN_HEADER_NAME = process.env.TOKEN_HEADER_NAME;

  readonly STORAGE_TMP = resolve(this.STORAGE_DIR, 'tmp');
  readonly STORAGE_PHOTOS = resolve(this.STORAGE_DIR, 'photos');
  readonly STORAGE_ASSETS = resolve(this.STORAGE_DIR, 'assets');

  // readonly PHOTOS_BASE_PATH = '/photos';
  readonly STORAGE_THUMBS = resolve(this.STORAGE_DIR, 'assets/photos');

  readonly DB_NAME = resolve(this.STORAGE_DIR, 'db.sql');

}
