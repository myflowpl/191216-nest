import { Module } from '@nestjs/common';
import { PhotosService } from './services';
import { ConfigModule } from '@app/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbModule } from '@app/db';
import { PhotoEntity } from '@app/db/entities';

@Module({
  imports: [
    ConfigModule,
    DbModule,
    TypeOrmModule.forFeature([PhotoEntity]),
  ],
  providers: [PhotosService],
  exports: [PhotosService],
})
export class PhotosCoreModule { }
